import * as express from 'express';
import FaqController from "../../controllers/faq";

const router = express.Router();
router.route('/getFaq').get(FaqController.getFaq);

export default router;
