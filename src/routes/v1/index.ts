import * as express from 'express';
import faq from '../v1/faq.routes';
import repositories from '../v1/repositories.routes';

const router = express.Router();
router.use('/faq', faq);
router.use('/repositories', repositories);

export default router;
