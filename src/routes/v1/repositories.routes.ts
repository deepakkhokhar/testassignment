import * as express from 'express';
import RepositoriesController from "../../controllers/repositories";

const router = express.Router();
router.route('/:ownerId/:repository/commits/:oid').get(RepositoriesController.getCommitById);
router.route('/:ownerId/:repository/commits/:oid/diff').get(RepositoriesController.getCommitDiff);

export default router;
