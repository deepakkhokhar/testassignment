import * as express from 'express';
import CustomResponse from '../utils/response';
import RepositoryDBService from "../database/service/repositories.dbservice";
/**
 *
 *
 * @export
 * @class RepositoriesController
 */
export default class RepositoriesController {

  static async getCommitById(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction

  ): Promise<void> {
    try {
      const ownerId = req.params.ownerId;
      const repository = req.params.repository;
      const oid = req.params.oid;
      const result = await RepositoryDBService.getCommitById(ownerId,repository,oid);
      const response = new CustomResponse(res);
      return response.setResponse({ result });
    }
    catch (error) {
      return next(error);
    }
  }

  static async getCommitDiff(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction

  ): Promise<void> {
    try {
      const ownerId = req.params.ownerId;
      const repository = req.params.repository;
      const oid = req.params.oid;
      const result = await RepositoryDBService.getCommitDiff(ownerId,repository,oid);
      const response = new CustomResponse(res);
      return response.setResponse({ result });
    }
    catch (error) {
      return next(error);
    }
  }
}
