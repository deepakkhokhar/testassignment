import * as express from 'express';
import CustomResponse from '../utils/response';
import FaqDBService from "../database/service/faq.dbservice";
/**
 *
 *
 * @export
 * @class ManagePagesController
 */
export default class FaqController {

  static async getFaq(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction

  ): Promise<void> {
    try {

      const result = await FaqDBService.getFaq();
      const response = new CustomResponse(res);
      return response.setResponse({ result });
    }
    catch (error) {
      return next(error);
    }
  }
}
