
import { Model, DataTypes } from 'sequelize';
import sequelize from '../connection';

class Faq extends Model {
  public id: number;
}

Faq.init({
  uuid: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    unique: true
  },
  question: {
    type: DataTypes.STRING(100)
  },
  answer: {
    type: DataTypes.STRING(400)
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  status: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  }
}, {
  modelName: 'faq',
  tableName: 'faq',
  sequelize,
  timestamps: true
});

export default Faq;
