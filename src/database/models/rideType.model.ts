
import { Model, DataTypes } from 'sequelize';
import sequelize from '../connection';

class RideType extends Model {
  public id: number;
}

RideType.init({
  uuid: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    unique: true
  },
  type: {
    type: DataTypes.STRING(20)
  },
  description: {
    type: DataTypes.STRING(100),
    defaultValue : null
  },
  isDeleted: {
    type: DataTypes.BOOLEAN,
    defaultValue: false
  },
  status: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  }
}, {
  modelName: 'rideType',
  tableName: 'rideType',
  sequelize,
  timestamps: true
});

export default RideType;
