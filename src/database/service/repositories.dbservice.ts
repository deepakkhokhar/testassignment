import db from '../index';
import CustomError from "../../utils/error";
import {errorManager} from "../../config/errorManager";

const {Faq} = db;
const serviceName = "[RepositoryService]";

export default class RepositoryDBService {
    static getCommitById(ownerId,repository,oid) {
        if(!ownerId || !repository || !oid){
            throw new CustomError({ serviceName, ...errorManager.NO_REPO_FOUND });
        }
        return Faq.findAll({
            where: {
                isDeleted: 0
            },
            raw: true,
            nest: true
        });
    }

    static getCommitDiff(ownerId,repository,oid) {
        if(!ownerId || !repository || !oid){
            throw new CustomError({ serviceName, ...errorManager.NO_REPO_FOUND });
        }
        return Faq.findAll({
            where: {
                isDeleted: 0
            },
            raw: true,
            nest: true
        });
    }
}

