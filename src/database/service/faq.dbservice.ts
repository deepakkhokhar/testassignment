import db from '../index';

const {Faq} = db;

export default class FaqDBService {
    static getFaq() {
        return Faq.findAll({
            where: {
                isDeleted: 0
            },
            raw: true,
            nest: true
        });
    }
}

