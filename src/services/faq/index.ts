import FaqDBService from "../../database/service/faq.dbservice";


const serviceName = '[FaqService]';

export default class FaqService {

    static async getFaq() {
        return await FaqDBService.getFaq();
    }

}
