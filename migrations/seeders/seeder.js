'use strict';

const uuidv1 = require('uuid/v1');
const bcrypt = require('bcryptjs');
const rawApis = require('./apis.json');
const rawPolicy = require('./policy.json');
const rawLanguages = require('./language.json');
const rawTemplates = require('./emailTemplates.json');
const rawManagePages = require('./managePages.json');
const rawCities = require('./cities.json');
const rawSms = require('./sms.json');
const rawOrganization = require('./organizations.json');
const rawRoles = require('./roles.json');
const paymentTypeJson = require('./paymentType.json');
const faqJson = require('./faq.json');
const rideTypeJson = require('./rideType.json');

const salt = process.env.SALT;

module.exports = {
  /*
  This function will seed all the required data in the OAuthClients table
 */
  // eslint-disable-next-line no-unused-vars
  up: async (queryInterface, Sequelize) => {
    const apis = [];
    const polices = [];
    const languages = [];
    const email = [];
    const policyAPIMapper = [];
    const emailTemplates = [];
    const managePages = [];
    const cities = [];
    const sms = [];
    const roles = [];
    const organizations = [];
    const rolesPolicyMapper = [];
    const paymentTypeArr = [];
    const faqArr = [];
    const rideType = [];

    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 0;');

    await queryInterface.bulkDelete('languages', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('managePages', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('cities', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('sms', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('emailTemplates', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('rolesPolicyMapper', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('applicationPolicies', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('applicationRoles', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('organizations', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('applicationApis', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('policyApiMapper', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('paymenttype', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('faq', {} ,{ truncate: true} );
    await queryInterface.bulkDelete('rideType', {} ,{ truncate: true} );

    await queryInterface.sequelize.query('SET FOREIGN_KEY_CHECKS = 1;');

    rawLanguages.forEach((obj) => {
      languages.push({
        uuid: uuidv1(),
        language: obj.language,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert('languages', languages, {} );

    rawPolicy.forEach((obj) => {
      polices.push({
        policyName: obj.policyName,
        policyDescription: obj.policyDescription,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert('applicationPolicies', polices, {});

    rawApis.forEach((obj) => {
      apis.push({
        uuid: uuidv1(),
        apiId: obj.apiId,
        apiEndpoint: obj.apiEndpoint,
        createdAt: new Date(),
        updatedAt: new Date()
      });
      obj.policyName.forEach((policy) => {
        policyAPIMapper.push({
          uuid: uuidv1(),
          policyName: policy,
          apiId: obj.apiId,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      });
    });
    queryInterface.bulkInsert('applicationApis', apis, {});

    rawTemplates.forEach((obj) => {
      emailTemplates.push({
        uuid: uuidv1(),
        type: obj.type,
        subject: obj.subject,
        htmlDescription: obj.htmlDescription,
        status: obj.status,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });

    rawManagePages.forEach((obj) => {
      managePages.push({
        uuid: uuidv1(),
        pageTitle: obj.pageTitle,
        description: obj.description,
        isDeleted: 0,
        status: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert( 'managePages', managePages, {} );


    /*concierge faq starts*/

    faqJson.forEach((obj) => {
      faqArr.push({
        uuid: uuidv1(),
        question: obj.question,
        answer: obj.answer,
        isDeleted: 0,
        status: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert( 'faq', faqArr, {} );

    /*concierge faq ends*/

    /*concierge ride type starts*/

    rideTypeJson.forEach((obj) => {
      rideType.push({
        uuid: uuidv1(),
        type: obj.type,
        isDeleted: 0,
        status: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert( 'rideType', rideType, {} );

    /*concierge ride type ends*/


    paymentTypeJson.forEach((obj) => {
      paymentTypeArr.push({
        uuid: uuidv1(),
        name: obj.name,
        status: obj.status,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert( 'paymenttype', paymentTypeArr, {} );

    rawCities.forEach((obj) => {
      cities.push({
        uuid: uuidv1(),
        name: obj.name,
        country: obj.country,
        countryCode: obj.countryCode,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert('cities', cities, {} );

    rawSms.forEach((obj) => {
      sms.push({
        uuid: uuidv1(),
        type: obj.type,
        body: obj.body,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert('sms', sms, {} );

    /* Entry for organization table */
    rawOrganization.forEach((obj) => {
      organizations.push({
        organizationId: obj.organizationId,
        organizationName: obj.organizationName,
        organizationInfo: obj.organizationInfo,
        createdAt: new Date(),
        updatedAt: new Date()
      });
    });
    queryInterface.bulkInsert('organizations', organizations, {} );

    /* Entry for applicationRoles and rolesPolicyMapper table */
    rawRoles.forEach((obj) => {
      const roleObj = {
        roleName: obj.roleName,
        roleDesc: obj.roleDesc,
        assignedPolicyCount: obj.policy.length,
        createdAt: new Date(),
        updatedAt: new Date()
      };
      if (obj.organizationId) {
        roleObj.organization_id = obj.organizationId;
      }
      roles.push(roleObj);
      obj.policy.forEach((policy) => {
        rolesPolicyMapper.push({
          uuid: uuidv1(),
          policyName: policy,
          roleName: obj.roleName,
          createdAt: new Date(),
          updatedAt: new Date()
        });
      });
    });

    queryInterface.bulkInsert('applicationRoles', roles, {});

    queryInterface.bulkInsert('rolesPolicyMapper', rolesPolicyMapper, {});

    queryInterface.bulkInsert('emailTemplates',emailTemplates,{});

    return queryInterface.bulkInsert('policyApiMapper', policyAPIMapper, {});


  },



  // eslint-disable-next-line no-unused-vars
  down: (queryInterface, Sequelize) => {
    /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('People', null, {});
        */
  }
};
